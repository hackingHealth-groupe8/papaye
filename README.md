# Papaye

Solution modulaire pour la gestion de documents numériques

## Contexte

Ce démonstrateur a été réalisé en 48h dans le cadre du [Hacking Health - Besançon 2017](http://hacking-health.org/besancon-fr/#hacking-health-besancon-hackathon).

Celui ci a été réfléchi pour proposer une réponse à la problématique de la [gestion des documents papiers](https://besanconhh.sparkboard.com/project/59ad814b98764504002c1a3e) au sein des laboratoires d'étude biologique.

Une instance de démonstration est disponible à l'adresse [http://papaye.dev.lookingfora.name/](http://papaye.dev.lookingfora.name/).

## Auteurs

- Sébastien Simonot, porteur de projet et assistant médicale de laboratoire pour l'[EFS](https://www.efs.sante.fr/)
- [Justin Loye](https://www.linkedin.com/in/justin-loye-66631a14a/), étudiant de l'université de Bourgogne Franche Comté
- [Antoine Baudiquez](https://www.linkedin.com/in/antoine-baudiquez-39731b14a/), étudiant de l'université de Bourgogne Franche Comté
- [Benjamin Gaudé](https://www.linkedin.com/in/bengaude/), ingénieur Logiciels Libres pour la [SCOP Cadoles](https://www.cadoles.com/)
- [William Petit](https://www.linkedin.com/in/willpetit/), ingénieur Logiciels Libres également pour la SCOP Cadoles.

## Licence

AGPL 3.0
