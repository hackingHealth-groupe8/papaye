(function(Papaye) {

  const BASE_URL = 'https://shelf.dev.lookingfora.name';

  Papaye.login = function(login, password) {
    return fetch(`${BASE_URL}/papaye.users/${login}`)
      .then(res => res.json())
      .then(res => {
        if(res.error) {
          Papaye.Store.update('user', null);
          return null;
        }
        const user = res.data.item;
        if (user.password !== password) {
          Papaye.Store.update('user', null);
          return null;
        }
        Papaye.Store.update('user', user);
        return user;
      })
    ;
  };

  Papaye.fetchCollection = function(coll, filter) {
    const filterQuery = filter ? '?f=' + JSON.stringify(filter) : '';
    return fetch(`${BASE_URL}/papaye.${coll}${filterQuery}`)
      .then(res => res.json())
      .then(res => {
        if(res.error) {
          Papaye.Store.update(coll, null);
          return null;
        }
        Papaye.Store.update(coll, res.data.items);
        return res.data.items;
      })
    ;
  };

  Papaye.fetchItem = function(coll, key) {
    return fetch(`${BASE_URL}/papaye.${coll}/${key}`)
      .then(res => res.json())
      .then(res => {
        if(res.error) return null;
        return res.data.item;
      })
    ;
  };

  Papaye.saveItem = function(coll, item) {
    return fetch(`${BASE_URL}/papaye.${coll}`, {
        method: 'POST',
        body: JSON.stringify(item)
      })
      .then(res => res.json())
      .then(res => {
        if(res.error) return null;
        return res.data.item;
      })
    ;
  }

}(window.Papaye = window.Papaye || {}))
