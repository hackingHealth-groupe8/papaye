<papaye-import-tile>
  <a class="tile-link" onclick="{ handleClick }">
    <papaye-tile ref="tile" header="Importer" border="border-warning" colortext="text-warning">
      <yield to="body">
        <input ref="file" style="display:none;" onchange={ parent.handleForm } type="file"/>
        <i class="fa fa-plus tile-number"></i>
      </yield>
    </papaye-tile>
  </a>

  <script>

      this.handleClick = function(event) {
          var fileInput = this.refs.tile.refs.file;
          var clickEvent = document.createEvent('MouseEvent');
          clickEvent.initMouseEvent ("click", false, false);
          fileInput.dispatchEvent(clickEvent);
      }

      this.handleForm = function(evt) {

        var fileInput = evt.target;
        var reader = new FileReader();

        reader.addEventListener('load', function() {

          var lines = reader.result.split('\n')
          var patients = [];

          for (var i =0; i < (lines.length)-5; i++){
            if (i <= 7) continue;
            var p = {};
            p["analyse"] = lines[5].slice(-7)
            p["id"] = lines[i].split(' ')[0]
            p["nom"] = lines[i].split('.')[1]
            p["nom"] = p["nom"].slice(6,(p["nom"].length)-2)
            patients.push(p)
          }

          Papaye.saveItem('listetravail', {analyses: patients});

        });

        reader.readAsText(fileInput.files[0]);
      }

  </script>
</papaye-import-tile>
