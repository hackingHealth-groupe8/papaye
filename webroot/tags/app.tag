<papaye-app>
  <router ref="router">
    <route path="/"><papaye-redirect path="/login"></papaye-redirect></route>
    <route path="/home">
      <papaye-home-page />
    </route>
    <route path="/doing-examens">
      <papaye-doing-examens-page />
    </route>
    <route path="/doing-analyse/*">
      <papaye-doing-analyse-page />
    </route>
    <route path="/login">
      <papaye-login-page />
    </route>
    <route path="/*/work">
      <papaye-work-page />
    </route>
    <route path="/paillasse/*/detail">
      <papaye-paillasse-detail-page />
    </route>
    <route path=".."><papaye-not-found-page /></route>
  </router>
</papaye-app>
