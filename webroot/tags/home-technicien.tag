<papaye-home-technicien-page>
  <section class="container-fluid mt-2 mb-2">
    <section class="row">
      <section class="col-12">
        <form class="form-row align-items-center">
          <div class="col-12 col-sm-11 mb-1 mb-sm-0">
            <input type="search" class="form-control" id="inlineFormInputGroup" placeholder="Rechercher une analyse" />
          </div>
          <div class="col-12 col-sm-1">
            <button type="submit" class="btn btn-primary btn-block">
              <i class="fa fa-search"></i>
            </button>
            </div>
        </form>
      </section>
    </section>
  </section>
  <section class="container-fluid mt-2">
    <section class="row">
      <section class="col-12 col-sm-4 mb-2 mb-sm-0">
        <papaye-import-tile></papaye-import-tile>
      </section>
      <section class="col-12 col-sm-8">
        <papaye-doing-tile></papaye-doing-tile>
      </section>
    </section>
  </section>
</papaye-home-technicien-page>
