<papaye-work-table>
  
  <section class="container-fluid">
    <section class="row">
      <section class="col-12 col-md-9">
        <h3 class="mt-4 mb-1 mb-md-4">Feuille de paillasse</h3>
      </section>
      <section class="col-12 col-md-3 mb-2 mb-md-0">
        <button type="button"  data-toggle="modal" data-target="#checklistModal" class="btn btn-block btn-lg mt-3 mb-4 btn-outline-warning">
          <i class="fa fa-list fa-fw"></i> Checklist
        </button>
      </section>
    </section>
  </section>
  

  <section class="table-overflow">
    <table class="table table-responsive">
      <thead>
        <tr class="bg-light">
          <th width="100px"></th>
          <th>Identité</th>
          <th>Observations</th>
          <th>Commentaires</th>
        </tr>
      </thead>
      <tbody class="work-table-body">
        <tr each={ i in opts.patients }>
          <td  width="100px" class="align-middle text-center">
            <label class="label">
              <input  class="label__checkbox" type="checkbox" disabled={ i == 1 } />
              <span class="label__text">
                <span class="label__check  { i == 1 ? 'disabled' : '' }">
                  <i class="fa fa-{ i == 1 ? 'lock' : 'check' } icon"></i>
                </span>
              </span>
            </label>
          </td>
          <td class="align-middle">
            <strong>Test patient {i}</strong>
            <span class="serial">123456789</span>
          </td>
          <td class="align-middle">
            <select class="form-control" disabled={ i == 1 }>
              <option value="1">-- Sélectionner --</option>
              <option value="1">Loci A et B à controler par BM</option>
              <option value="1">Typage HLA de classe 1 par RSSO Luminex</option>
              <option value="1">Typage HLA de classe 2 par RSSO Luminex</option>
              <option value="1">Déséquilibre B-C OK</option>
              <option value="1">Déséquilibre B-C peu fréquent/rare</option>
            </select>
          </td>
          <td class="align-middle">
            <textarea name="name" class="form-control" disabled={ i == 1 } class="w-100"></textarea>
          </td>
        </tr>
      </tbody>
    </table>
  </section>

  
  <nav class="navbar navbar-expand-md fixed-bottom navbar-light bg-light">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
        <ul class="navbar-nav mr-auto mt-2 mt-sm-0 mb-2 mb-sm-0">
          <li class="nav-item">
            <button type="button" class="btn btn-lg btn-outline-info"><i class="fa fa-check-circle fa-fw"></i> Tout cocher</button>
          </li>
          <!--<li class="nav-item">
            <button type="button" class="btn btn-lg btn-outline-info"><i class="fa fa-check-circle-o fa-fw"></i> Tout decocher</button>
          </li>-->
        </ul>
        <ul class="navbar-nav ml-auto">
          <li class="nav-item mr-2 mb-2 mb-sm-0">
            <button class="btn btn-lg btn-outline-success" data-toggle="modal" data-target="#passphraseModal"><i class="fa fa-paper-plane-o fa-fw"></i> Valider & envoyer</button>
          </li>
          <li class="nav-item">
            <button type="button" class="btn btn-lg btn-outline-danger"><i class="fa fa-bolt fa-fw"></i> Urgent</button>
          </li>
        </ul>
      </div>
  </nav>
  
<!-- Password Modal -->
<div class="modal fade" id="passphraseModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Mot de passe</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="passphrase">Entrez votre mot de passe</label>
            <input type="password" class="form-control">
            <small id="emailHelp" class="form-text text-muted">Ce mot de passe est utilisé pour signer le document</small>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
        <button type="button" class="btn btn-success"><i class="fa fa-paper-plane-o fa-fw"></i> Valider & envoyer</button>
      </div>
    </div>
  </div>
</div>

<!-- Checklist Modal -->
<div class="modal fade" id="checklistModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Luminex: Checklist d'une série de recherche des anticorps anti-HLA par technique<br/> &laquo; LabScreen &raquo;</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <table class="table">
        <tbody class="checklist-table-body">
          <tr>
            <td colspan="2" class="font-weight-bold bg-info text-white">Prise de poste</td>
          </tr>
          <tr>
            <td width="100px" class="align-middle text-center">
              <label class="label">
                <input  class="label__checkbox" type="checkbox" />
                <span class="label__text">
                  <span class="label__check">
                    <i class="fa fa-check icon"></i>
                  </span>
                </span>
              </label>
            </td>
            <td class="align-middle">
              Vérification de l’intégralité des échantillons à traiter
            </td>
          </tr>
          <tr>
            <td  width="100px" class="align-middle text-center">
              <label class="label">
                <input  class="label__checkbox" type="checkbox" />
                <span class="label__text">
                  <span class="label__check">
                    <i class="fa fa-check icon"></i>
                  </span>
                </span>
              </label>
            </td>
            <td class="align-middle">
              Destruction des tubes correspondant à la série S-2
            </td>
          </tr>
          <tr>
            <td colspan="2" class="font-weight-bold bg-info text-white">Adsorb out</td>
          </tr>
          <tr>
            <td width="100px" class="align-middle text-center">
              <label class="label">
                <input  class="label__checkbox" type="checkbox" />
                <span class="label__text">
                  <span class="label__check">
                    <i class="fa fa-check icon"></i>
                  </span>
                </span>
              </label>
            </td>
            <td class="align-middle">
              Vérifier les échantillons à traiter par Adsorb Out
            </td>
          </tr>
          <tr>
            <td  width="100px" class="align-middle text-center">
              <label class="label">
                <input  class="label__checkbox" type="checkbox" />
                <span class="label__text">
                  <span class="label__check">
                    <i class="fa fa-check icon"></i>
                  </span>
                </span>
              </label>
            </td>
            <td class="align-middle">
              Traiter les sérums: 3 µL Adsorb Out  + 30 µL sérum, vortexer
            </td>
          </tr>
        </tbody>
      </table>
      <div class="modal-footer">
        <button type="button" class="btn btn-lg btn-outline-secondary" data-dismiss="modal">Fermer</button>
        <button type="button" class="btn btn-outline-success btn-lg"><i class="fa fa-save fa-fw"></i>Enregistrer</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  this.opts.patients = [1,2,3,4,5,6,7,8,9];
  
</script>
</papaye-work-table>