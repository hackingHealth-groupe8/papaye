<papaye-home-biologiste-page>
  
  <section class="container-fluid mt-2 mb-2">
    <section class="row">
      <section class="col-12">
        <h3>Panneau de contrôle</h3>
      </section>
    </section>
  </section>
  
  
  <section class="container-fluid mt-2 mb-2">
    <section class="row">
      <section class="col-6">
        <papaye-panel header="À valider">
          <yield to="body">
            <table class="table">
              <thead>
                <tr>
                  <th>Paillasse</th>
                  <th>Technicien</th>
                  <th>Notifié le</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="align-middle">
                    <a href="#/paillasse/123456789/detail">123456789</a>
                  </td>
                  <td class="align-middle">
                    John Doe
                  </td>
                  <td class="align-middle">
                    14/10/2017
                  </td>
                </tr>
              </tbody>
            </table>
          </yield>
        </papaye-panel>
      </section>
      <section class="col-6">
        <papaye-panel header="Validation urgente">
          <yield to="body">
            <table class="table">
              <thead>
                <tr>
                  <th>Paillasse</th>
                  <th>Technicien</th>
                  <th>Notifié le</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="align-middle">
                    <a href="#/paillasse/123456789/detail">123456789</a>
                  </td>
                  <td class="align-middle">
                    John Doe
                  </td>
                  <td class="align-middle">
                    14/10/2017
                  </td>
                </tr>
              </tbody>
            </table>
          </yield>
        </papaye-panel>
      </section>
    </section>
  </section>
  
</papaye-home-biologiste-page>
