<papaye-navbar>
  <nav class="navbar navbar-expand-sm navbar-light bg-light">
    <a class="navbar-brand text-uppercase" href="#/home">Papaye!</a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <span class="navbar-text">{ opts.user.username } { opts.user._key == "bio" ? "(Biologiste)" : "(Technicien)" }</span>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item ">
          <a class="nav-link notification" href="#/notifications">
            <i class="fa fa-bell-o"></i><span class="number">8</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">
            <i class="fa fa-power-off"></i>
          </a>
        </li>
      </ul>
    </div>
  </nav>
  <script>
    this.on('mount', () => {
      if(!Papaye.Store.user) route('/login');
    });
    Papaye.Store.on('user', user => {
      this.opts.user = user;
      this.update();
    });
    this.on('before-mount', () => {
      this.opts.user = Papaye.Store.user || {};
    });
  </script>
</papaye-navbar>
