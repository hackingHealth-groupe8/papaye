<papaye-doing-examens-page>

  <papaye-navbar></papaye-navbar>

  <section class="container-fluid">
    <section class="row">
      <section class="col-12">
        <h3 class="mt-4 mb-4">Examens en cours</h3>
        <ul class="list-group">
          <li class="list-group-item" each={ opts.identification }>
            <a href="#/doing-analyse/{ _key }">{ Label }</a>
          </li>
        </ul>
      </section>
    </section>
  </section>

  <script>
    Papaye.Store.on('identification', identification => {
      Papaye.fetchCollection('analyse')
        .then(coll => {
          const identificationSet = coll.reduce((identificationSet, analyse) => {
            identificationSet.add(analyse.Identification)
            return identificationSet;
          }, new Set());
          this.opts.identification = identification.filter(ident => identificationSet.has(ident._key));
          this.update();
        })
      ;
    });
    this.on('before-mount', () => {
      this.opts.identification = [];
      Papaye.fetchCollection('identification');
    });

  </script>
</papaye-doing-examens-page>
