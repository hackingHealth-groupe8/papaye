<papaye-login-page>

  <section class="container" style="height:100vh;">
    <section class="row align-items-center">
      <!--<section class="d-flex flex-row justify-content-center align-items-center">-->
        <section class="col-12 col-lg-6 mt-4 order-xs-2">
          <img src="http://hacking-health.org/wp-content/uploads/2017/06/hhlogonew.png" alt="Hacking Health" style="width:100%" />
          <papaye-panel ref="authpanel">
            <yield to="header">
              <i class="fa fa-lock papaye-second-color"></i> PAPAYE - Authentification
            </yield>
            <yield to="body">
              <div if="{ parent.opts.authError }" class="alert alert-danger" role="alert">
                Mauvais identifiants
              </div>
              <form onsubmit={ parent.submit }>
                <div class="form-group">
                  <label for="uid">Identifiant</label>
                  <input type="text" ref="login" class="form-control" id="uid" placeholder="Identifiant" onkeyup={ parent.updateLogin } />
                </div>
                <div class="form-group">
                  <label for="pwd">Mot de passe</label>
                  <input type="password" ref="password" class="form-control" id="pwd" placeholder="Mot de passe" onkeyup={ parent.updatePassword } />
                </div>
                <button type="submit" class="btn btn-primary float-right">Connexion</button>
              </form>
            </yield>
          </papaye-panel>
          <a href="./don-sang/" class="mt-2 float-right btn btn-info btn-sm">Démo formulaire don du sang <i class="fa fa-chevron-right"></i></a>
        </section>
        <section class="col-12 col-lg-6 order-xs-1">
          <div class="alert alert-light" role="alert" style="font-size:0.9em;">
            <br />
            <h5>Papaye, solution modulaire pour la gestion de documents numériques</h5>
            <p>Ce prototype d'application a été réalisé en 48h dans le cadre du <a target="_blank" href="http://hacking-health.org/besancon-fr/#hacking-health-besancon-hackathon">Hacking Health - Besançon 2017</a>.</p>
            <p>Celui ci a été réfléchi pour proposer une réponse à la problématique de la <a href="https://besanconhh.sparkboard.com/project/59ad814b98764504002c1a3e">gestion des documents papiers</a> au sein des laboratoires d'étude biologique.</p>
            <p><b>Réalisation</b></p>
            <ul>
              <li>Sébastien Simonot, porteur de projet et assistant médical de laboratoire pour l'<a target="_blank" href="https://www.efs.sante.fr/">EFS</a></li>
              <li><a target="_blank" href="https://www.linkedin.com/in/justin-loye-66631a14a/">Justin Loye</a>, étudiant de l'université de Bourgogne Franche Comté</li>
              <li><a target="_blank" href="https://www.linkedin.com/in/antoine-baudiquez-39731b14a/">Antoine Baudiquez</a>, étudiant de l'université de Bourgogne Franche Comté</li>
              <li><a target="_blank" href="https://www.linkedin.com/in/bengaude/">Benjamin Gaudé</a>, ingénieur Logiciels Libres pour la <a target="_blank" href="https://www.cadoles.com/">SCOP Cadoles</a></li>
              <li><a target="_blank" href="https://www.linkedin.com/in/willpetit/">William Petit</a>, ingénieur Logiciels Libres également pour la SCOP Cadoles.</li>
            </ul>
            <p><b>Comptes utilisateurs de test</b></p>
            <p>
              Deux comptes utilisateurs sont disponibles pour tester ce prototype.
            </p>
            <ul>
              <li><b>tech</b>/<b>tech</b> Rôle technicien de laboratoire</li>
              <li><b>bio</b>/<b>bio</b> Rôle biologiste</li>
            </ul>
            <p><b>Licence</b></p>
            <p>Le code source de cette application est publié sous licence libre <a href="https://gitlab.com/hackingHealth-groupe8/papaye/blob/develop/LICENSE"><strong>AGPL 3.0</strong></a> et est <a href="https://gitlab.com/hackingHealth-groupe8/papaye">disponible ici</a>.</p>
          </div>
        </section>
      <!--</section>-->
    </section>
  </section>



  <script>
    Papaye.Store.on('user', () => this.update());
    this.on('before-mount', () => {
      this.opts.login = '';
      this.opts.password = '';
    });
    submit (e) {
      e.preventDefault();
      Papaye.login(this.opts.login, this.opts.password)
        .then(user => {
          if(user == null) {
            this.opts.authError = true;
            this.refs.authpanel.refs.login.value = '';
            this.refs.authpanel.refs.password.value = '';
            this.opts.login = '';
            this.opts.password = '';
            this.update();
            console.log("Invalid password !")
            return;
          }
          route('/home');
          return;
        })
        .catch(err => {
          this.opts.authError = true;
          this.opts.login = '';
          this.opts.password = '';
          this.refs.authpanel.refs.login.value = '';
          this.refs.authpanel.refs.password.value = '';
          this.update();
          console.error(err)
        })
      ;
    }
    updatePassword(e) {
      this.opts.password = e.target.value;
    }
    updateLogin(e) {
      this.opts.login = e.target.value;
    }
  </script>

</papaye-login-page>
