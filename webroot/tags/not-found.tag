<papaye-not-found-page>
  <section class="d-flex flex-column justify-content-center align-items-center" style="height:100vh;">
    <p>La page que vous recherchez n'a pu être trouvée.</p>
    <p><a class="btn btn-success" href="#/"><i class="fa fa-undo fa-fw"></i> Retourner à la page d'accueil</a></p>
  </section>
</papaye-not-found-page>