<papaye-doing-analyse-page>

  <papaye-navbar></papaye-navbar>

  <section class="container-fluid">
    <section class="row">
      <section class="col-12">
        <h3 class="mt-4 mb-4">Analyses pour "{ opts.identification.Label }"</h3>
      </section>
    </section>
  </section>

  <section class="table-overflow">
    <table class="table">
      <thead>
        <tr class="bg-light">
          <th width="100px"></th>
          <th>Analyses</th>
        </tr>
      </thead>
      <tbody>
        <tr each={ opts.analyse }>
          <td  width="100px" class="align-middle text-center">
            <label class="label">
              <input  class="label__checkbox" type="checkbox" checked={ parent.opts.selectedAnalyses.has(_key) } />
              <span class="label__text">
                <span class="label__check">
                  <i class="fa fa-check icon"></i>
                </span>
              </span>
            </label>
          </td>
          <td class="col-auto align-middle">
            <a href="#/25/work" class="examens-identity">
              { Name }
              <span class="serial">{ Identifiant }</span>
            </a>
          </td>
        </tr>
      </tbody>
    </table>
  </section>

  <nav class="navbar navbar-expand-sm fixed-bottom navbar-light bg-light">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarActionsContent" aria-controls="navbarActionsContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarActionsContent">
        <ul class="navbar-nav mr-auto mt-2 mt-sm-0 mb-2 mb-sm-0">
          <li class="nav-item">
            <button type="button" if={ opts.tickAllBtn } onclick={ tickAll } class="btn btn-lg btn-outline-info"><i class="fa fa-check-circle fa-fw"></i> Tout cocher</button>
          </li>
          <li class="nav-item">
            <button type="button" if={ opts.untickAllBtn } onclick={ untickAll } class="btn btn-lg btn-outline-info"><i class="fa fa-check-circle-o fa-fw"></i> Tout decocher</button>
          </li>
        </ul>
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <button onclick={ combineAndLoad } type="button" class="btn btn-lg btn-outline-success"><i class="fa fa-code-fork fa-fw"></i> Combiner & traiter</button>
          </li>
        </ul>
      </div>
  </nav>

  <script>
    this.opts.tickAllBtn = true;
    this.on('route', identificationKey => {
      Papaye.fetchCollection('analyse', {eq: {"Identification": identificationKey}});
      Papaye.fetchItem('identification', identificationKey)
        .then(identification => {
          this.opts.identification = identification;
          this.update();
          return ;
        })
      ;
    });
    Papaye.Store.on('identification', identification => {
      this.opts.identification = identification;
      this.update();
    });
    Papaye.Store.on('analyse', analyse => {
      this.opts.analyse = analyse || [];
      this.update();
    });
    this.on('before-mount', () => {
      this.opts.analyse = [];
      this.opts.selectedAnalyses = new Set();
      this.opts.identification = this.opts.identification || {};
      if (this.opts.identification._key) Papaye.fetchCollection('analyse', {eq: {"Identification": this.opts.identification._key}})
    });

    onTickChange(e) {
      this.opts.selectedAnalyses.add(e.item._key);
      this.update();
    }

    tickAll() {
      this.opts.analyse.forEach(a => this.opts.selectedAnalyses.add(a._key));
      this.opts.tickAllBtn = false;
      this.opts.untickAllBtn = true;
      this.update();
    }

    untickAll() {
      this.opts.analyse.forEach(a => this.opts.selectedAnalyses.delete(a._key));
      this.opts.tickAllBtn = true;
      this.opts.untickAllBtn = false;
      this.update();
    }

  </script>


</papaye-doing-analyse-page>
