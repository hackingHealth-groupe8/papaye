<papaye-tile>
  <div class="card text-right { opts.border } ">
    <div class="card-body { opts.colortext }">
        <h4 class="card-title text-uppercase text-left">{ opts.header }</h4>
        <yield from="body" />
    </div>
  </div>
</papaye-tile>
