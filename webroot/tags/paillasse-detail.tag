<papaye-paillasse-detail-page>
  <papaye-navbar></papaye-navbar>
  
  <section class="container-fluid mt-2 mb-2">
    <section class="row">
      <section class="col-12">
        <h3>Détails de la paillasse N° { opts.identificationKey }</h3>
      </section>
    </section>
  </section>
  
  <table class="table">
    <thead>
      <tr class="bg-light">
        <th width="100px"></th>
        <th>Identité</th>
        <th>Observations</th>
        <th>Commentaires</th>
      </tr>
    </thead>
    <tbody class="work-table-body">
      <tr>
        <td  width="100px" class="align-middle text-center">
          <label class="label">
            <input  class="label__checkbox" type="checkbox"/>
            <span class="label__text">
              <span class="label__check">
                <i class="fa fa-check icon"></i>
              </span>
            </span>
          </label>
        </td>
        <td class="align-middle">
          <strong>Jane Doe</strong>
          <span class="serial">123456789</span>
        </td>
        <td class="align-middle">
          Déséquilibre B-C peu fréquent/rare
        </td>
        <td class="align-middle">
          A verifier par nouvelle technique
          <textarea name="name" class="form-control" class="w-100" placeholder="Déposer un commentaire"></textarea>
        </td>
      </tr>
    </table>
    
    <nav class="navbar navbar-expand-sm fixed-bottom navbar-light bg-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <ul class="navbar-nav ml-auto">
          <li class="nav-item mr-2">
            <button class="btn btn-lg btn-outline-success" data-toggle="modal" data-target="#passphraseModal"><i class="fa fa-paper-plane-o fa-fw"></i> Valider & envoyer</button>
          </li>
        </ul>
    </nav>
    
    <!-- Password Modal -->
    <div class="modal fade" id="passphraseModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Mot de passe</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form>
              <div class="form-group">
                <label for="passphrase">Entrez votre mot de passe</label>
                <input type="password" class="form-control">
                <small id="emailHelp" class="form-text text-muted">Ce mot de passe est utilisé pour signer le document</small>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
            <button type="button" class="btn btn-success"><i class="fa fa-paper-plane-o fa-fw"></i> Valider & envoyer</button>
          </div>
        </div>
      </div>
    </div>
    
  <script>
    this.on('route', identificationKey => {
      this.opts.identificationKey = identificationKey;
      this.update();
    });
  </script>
</papaye-paillasse-detail-page>