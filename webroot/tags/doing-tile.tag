<papaye-doing-tile>
  <a href="#/doing-examens" class="tile-link">
    <papaye-tile header="Examens en cours"  border="border-primary" colortext="text-primary">
      <yield to="body">
        <strong class="tile-number">{ parent.opts.examensEnCours }</strong>
      </yield>
    </papaye-tile>
  </a>
  <script>
    this.on('before-mount', () => {
      this.opts.examensEnCours = 0;
      Papaye.fetchCollection('analyse')
        .then(coll => {
          const identificationSet = coll.reduce((identificationSet, analyse) => {
            identificationSet.add(analyse.Identification)
            return identificationSet;
          }, new Set());
          this.opts.examensEnCours = identificationSet.size;
          this.update();
        })
      ;
    });
  </script>
</papaye-doing-tile>
