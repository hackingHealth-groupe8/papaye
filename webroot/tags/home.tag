<papaye-home-page>
  <papaye-navbar></papaye-navbar>
  
  <papaye-home-technicien-page if={ opts.user._key == "tech" }></papaye-home-technicien-page>
  <papaye-home-biologiste-page if={ opts.user._key == "bio" }></papaye-home-biologiste-page>
  
  <script>
    this.on('mount', () => {
      if(!Papaye.Store.user) route('/login');
    });
    Papaye.Store.on('user', user => {
      this.opts.user = user;
      this.update();
    });
    this.on('before-mount', () => {
      this.opts.user = Papaye.Store.user || {};
    });
  </script>
</papaye-home-page>
