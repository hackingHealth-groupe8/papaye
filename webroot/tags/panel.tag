<papaye-panel>
  <div class="card">
    <div class="card-body">
        <h4 class="card-title">{ opts.header }<yield from="header" /></h4>
        <yield from="body" />
    </div>
  </div>
</papaye-panel>
